IDIR = include
CC=gcc
CFLAGS=-I$(IDIR) -g

ODIR=bin

LIBS = -lsqlite3

MKDIR_P = mkdir -p

_DEPS = maskin.h stack.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = loadProgram.o sysCall.o execute.o fetch.o main.o stack.o softInt.o sigHandle.o sqlError.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

$(ODIR)/%.o: %.c $(DEPS)
	$(CC) -c -o $@ $< $(CFLAGS)

$(ODIR)/maskin: $(OBJ)
	$(CC)  -o $@ $^ $(CFLAGS) $(LIBS)

all: directories $(ODIR)/maskin

.PHONY: clean directories

directories: $(ODIR)

${ODIR}:
	$(MKDIR_P) $(ODIR)

clean:
	  rm -f $(ODIR)/*.o *~
