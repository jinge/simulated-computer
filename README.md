# Simulated Computer

This is a simulated computer which takes instructions from a file
written in its own assembly language. The assembly 
code is written in hex. A sample
file is provided and is called "minne"

This code is based on code written by Thomas Nordli.
Thomas Nordli's code is based on an example in the book
"Operating Systems - Internals and Design Principles" 
published by pearson and written by Richard Stallings.
The original unmodified code is provided as "unmodified.tar.gz"

Run instructions
.......................
Use operate.sh or ./maskin < memory-file


Make instructions
......................

make all
make clean

depends
.....................
lsqlite3
