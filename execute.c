#include <asm/unistd_64.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include "stack.h"
#include "maskin.h"
void execute(struct maskin *m, Stack *s, Stack *ms)
{

  /* 
     0001 (1) - Laste til AC fra minne
     0010 (2) - Lagre fra AC til minne
	 0011 (3) - AND operator AC & minne
	 0100 (4) - OR operator AC & minne
     0101 (5) - Addere til AC fra minne
	 0110 (6) - Print frem til nullterminator
	 0111 (7) - Push til stack og hent neste fra minne
	 1000 (8) - Pull fra stack og sett pc til denne verdi
	 1001 (9) - Interrupt mekanisme 1. Print interrupt til stdout
	 1010 (10) - Syscall
	 1110 (14) - Last fra adr til eax og eab
     1111 (15) - Exit med verdi fra AC
  */
  unsigned int op  = (m->ir) / 0x1000; //4096
  unsigned int adr = (m->ir) % 0x1000; //4096

	//Sets protected bit for operations 9 & 10
  if(op == 10 || op == 9)
	  m->prot = 1;

	//Checks if adress space is accessed out of prot mode
  if(adr == 0x500 || adr == 0x600 || adr == 0x700)
  {
	  if(m->prot != 1)
	  {
		  printf("Memory acces violation in %x\n", adr);
		  m->pc++;
		  exit(1);
	  }
  }

  switch (op) {
  case 1:
    m->ac = m->minne[adr];  break;
  case 2:                     
    m->minne[adr] = m->ac;  break;
  case 3:
    m->ac = m->ac & m->minne[adr]; break;
  case 4:
	m->ac = m->ac | m->minne[adr]; break;
  case 5:                     
    m->ac += m->minne[adr]; break;
  case 6:
	while(1)
	{
		fflush(stdout);
		printf("%c",m->minne[adr]);
		adr++;
		if(m->minne[adr] == 0)
		{
			printf("\n");
			break;
		}
		fflush(stdout);
	}
    break;
  case 7:
	//CALL
	push(s, ++m->pc);
	m->pc=adr;
	break;
  case 8:
	//RET
	m->pc=pop(s);
	break;
  case 9:
	//Push registere til stack
	push(ms,m->pc);push(ms,m->ac);push(ms,m->ir);
	interrupt(adr, m, s, ms);
	m->prot = 0;
	//pop registere fra stack
	m->ir = pop(ms);m->ac = pop(ms);m->pc = pop(ms);
	break;
  case 10:
	//Hent registerverdier fra minne
	m->eab = m->minne[adr];
	m->eax = m->minne[adr+1];
	//utfør
	sysCall(m->eab,m->eax,m, 0);
	m->prot=0;
	break;
  case 15:                     
    exit(0);
  default:
    printf("Ignorerer %x %x\n", op, adr);
    fflush(stdout);
  }
}
