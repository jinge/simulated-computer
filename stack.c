#include "stack.h"
#include "maskin.h"
#include <stdio.h>
void push(Stack *s, int item)
{
	if (s->tos == s->size)
	{
		printf("Full stack \n");
	}
	else
	{
		s->stack[++s->tos] = item;
	}
}
int pop(Stack *s)
{
	if (s->tos < 0)
	{
		printf("Stack underflow\n");
		return -1;
	}
	else
	{
		return s->stack[s->tos--];
	}
}
int peek(Stack *s)
{
	if (s->tos > -1)
	{
		printf("peek: %d\n", s->stack[s->tos]);
	}
	return 0;
}
