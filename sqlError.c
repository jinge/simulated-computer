#include <sqlite3.h>
#include <stdio.h>

int sqlError(sqlite3 *db)
{
	fprintf(stderr, "SQL error: %s\n", sqlite3_errmsg(db));
	sqlite3_close(db);
	sqlite3_free(sqlite3_errmsg);
	return 1;
}

