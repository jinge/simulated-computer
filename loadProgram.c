#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "maskin.h"

void load_program(struct maskin *m){

  unsigned int adr, instr;

  while ( EOF != scanf("%x%x\n", &adr, &instr) )
	{
    m->minne[adr] = instr;
	}
}
