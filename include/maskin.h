#include <sqlite3.h>
struct maskin 
{
  unsigned int pc;
  unsigned int ac;
  unsigned int ir;
	unsigned int prot;
	unsigned int eab;
	unsigned int eax;
  unsigned int minne[0xA00];
};

void sqlError(sqlite3 *db);
void load_program(struct maskin *m);
void execute(struct maskin *m, Stack *s, Stack *ms);
void fetch(struct maskin *m);
void interrupt(int signal, struct maskin *m , Stack *s, Stack *ms);
int sysCall(int eab,int eax, struct maskin *m, int time);
int sig_handler(int signo, struct maskin *m);
