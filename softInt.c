#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "stack.h"
#include "maskin.h"
void interrupt(int signal, struct maskin *m, Stack *s, Stack *ms)
{
	char* aniarr[] = {"|", "/", "-", "\\"};

	int interrupts[3] = {0x500, 0x600, 0x700};

	struct timespec tv;
	tv.tv_sec = 0;
	tv.tv_nsec = 2.5e+8;

	switch(signal)
	{
		case 1:
			{
				m->ir=interrupts[0]+0x6000;
				execute(m, s, ms);
				break;
			}
		case 2:
			{
				int t = 0;
				int ani = 0;
				printf("Sleeping for 10 sec ");
				fflush(stdout);
				while(t <= 50)
				{
					printf("\b%s", aniarr[ani]);
					t++;
					nanosleep(&tv, &tv);
					fflush(stdout);
					if(ani == 3)ani=0;
					else ani++;
				}
				printf("\n");
				break;
			}
				
		default:
			{
				printf("Unknown signal\n");
			}
		}
}
