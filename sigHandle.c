#include <stdio.h>
#include <stdlib.h>
#include <sqlite3.h>
#include <signal.h>
#include <string.h>
#include "stack.h" 
#include "maskin.h"

sqlite3 *db;
char *err_msg = 0;
char buffer[2560];

int sig_handler(int signo, struct maskin *m)
{
	if(signo == SIGHUP)
	{
		int rc = sqlite3_open("maskin.db", &db);
		printf("HUP!\n");

		if(rc != SQLITE_OK)
			sqlError(db);

		char *zSql = "SELECT * FROM Maskintilstand" 
								 " WHERE ID = 1;";
		sqlite3_stmt *pStmt;
		sqlite3_prepare_v2(db,zSql, -1, &pStmt, NULL);
		rc = sqlite3_step(pStmt);
		if(rc == SQLITE_ROW)
		{
			m->ac = (int)sqlite3_column_int(pStmt, 1);
			m->pc = (int)sqlite3_column_int(pStmt, 2);
			m->ir = (int)sqlite3_column_int(pStmt, 3);
			m->prot = (int)sqlite3_column_int(pStmt, 4);
			m->eab = (int)sqlite3_column_int(pStmt, 5);
			m->eax = (int)sqlite3_column_int(pStmt, 6);
			unsigned int *catch = (unsigned int*)sqlite3_column_blob(pStmt, 7);
			for(int i = 0; i < 2560; i++)
			{
				m->minne[i] = catch[i];
			}

		}
		else
			sqlError(db);
	}

	else if (signo == SIGTERM)
	{
		int rc = sqlite3_open("maskin.db", &db);

		if (rc != SQLITE_OK)
		{
			sqlError(db);
		}

		char *sql =
			"DROP TABLE IF EXISTS Maskintilstand;" \
			"CREATE TABLE Maskintilstand ( " \
			"id INTEGER PRIMARY KEY AUTOINCREMENT,"\
			"ac INTEGER   NOT NULL ,"\
			"pc INTEGER   NOT NULL ,"\
			"ir INTEGER   NOT NULL ,"\
			"prot INTEGER NOT NULL ,"\
			"eab INTEGER  NOT NULL ,"\
			"eax INTEGER  NOT NULL ,"\
			"minne BLOB);";

		rc = sqlite3_exec(db, sql, 0, 0, &err_msg);

		if (rc != SQLITE_OK )
		{
			sqlError(db);
		}
		
			snprintf(buffer, sizeof(buffer), 
					"INSERT INTO Maskintilstand "\
					"(ac,pc,ir,prot,eab,eax)"\
					" VALUES(%i,%i,%i,%i,%i,%i);"
					 ,m->ac,m->pc,m->ir,m->prot,m->eab,m->eax);

			sql = buffer;
			rc = sqlite3_exec(db, sql, 0, 0, &err_msg);

		if ( rc != SQLITE_OK )
		{
			sqlError(db);
		}

		char *zSql = "UPDATE Maskintilstand" 
			          " SET minne = ? "
								 "WHERE ID = 1;";
		sqlite3_stmt *pStmt;
		sqlite3_prepare_v2(db,zSql, -1, &pStmt, NULL);
		rc = sqlite3_bind_blob(pStmt, 1, m->minne, sizeof(m->minne),  NULL);
		sqlite3_step(pStmt);
	}

	return 0; //for everything except SIGTERM
}
