#include <stdio.h>
#include <unistd.h>
#include <sys/syscall.h>
#include <errno.h>
#include "stack.h"
#include "maskin.h"
int sysCall(int eab,int eax, struct maskin *m, int time)
{
	if(m->prot==1)
	{
		if(time != 0)
		{
		  syscall(eab,eax);
		  return 0;
		}
		else
		{
			syscall(eab,eax,time);
			return 0;
		}
	}
	else
	{
		printf("Not in kernel mode.\n");
		return 1;
	}
}
