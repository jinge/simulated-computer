#include <signal.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include "stack.h"
#include "maskin.h"


int main() {

	//Signal handler
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_handler = sig_handler;
	sigaction(SIGTERM, &action, NULL);
	sigaction(SIGHUP, &action, NULL);

    struct maskin m;
  
    // Laster program og data i minne fra standard inngang
    load_program(&m); 

    // Setter startpunkt
  m.pc = 0x300;
	m.prot = 0;
	Stack s;
	Stack ms;

	s.tos=-1;
	s.size=0xf;

	ms.tos=-1;
	ms.size=0xf;

    // hent-og-utfoer-syklus
    while (1) {
      fetch(&m);
      execute(&m,&s,&ms);
    }
  
    return 1; // dette skjer aldri
  }
