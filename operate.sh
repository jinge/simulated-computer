#!/bin/bash
#Sjekk om miljovariabel er satt
if [ -z ${MASKIN_STI} ]
then 
	#Sett variabel til 1 dersom miljovariabel ikke er satt
	envVar=1
else
	#Kjor resultat av maskinkodeprogram
	./maskin < $MASKIN_STI
	echo "Resultat: " $?
	##Sjekk for feilkode
    #if [ $? -eq 1 ]	
	#then
	#	echo "Feil."
	#fi
fi
#Dersom ikke vi har miljovariabel, men har kommandolinjeargument
if [ $envVar  ] && [ $1 > 0 ]
then
    ./maskin < $1
	echo "Resultat: " $?
	exit
	##Sjekk for feilkode
    #if [ $? -eq 1 ]	
	#then
	#	echo "Feil."
	#fi
fi
#Dersom hverken miljovariabel, eller kommandolinjeargument
if [ $envVar ] && [ -Z  $1 ]
then
	#Spør om og les inn variabel fra STDIN
	while :
	do
		echo "Tast inn sti til maskinkodeprogram eller x for å avslutte: "
		read path
		case $path in
			"x") break
		esac
		./maskin < $path 
		echo "Resultat: " $?
	done
fi
